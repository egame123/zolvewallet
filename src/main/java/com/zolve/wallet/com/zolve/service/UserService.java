package com.zolve.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zolve.entity.User;
import com.zolve.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	
	public User save(User user) throws Exception {
		if(user==null || user.getPhone()==null)
			throw new Exception("user or phone shouldn't be null");
		if(findUserById(user.getPhone())!=null)
			throw new Exception("User with id: "+user.getPhone()+" already exists");
    	return userRepository.save(user);
	}
    
    public User findUserById(String id) throws Exception {
    	if(id==null)
    		throw new Exception("User phone can't be null");
    	 return userRepository.findUserById(id);
    }
    
}
