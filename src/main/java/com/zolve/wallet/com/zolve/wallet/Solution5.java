package com.zolve.wallet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
// class Solution5 {
//	public static void main(String[] args) {
//		System.out.println(new Solution5().combine(1, 1));
//	}
//    public List<List<Integer>> combine(int n, int k) {
//        return combine(n, k, 1, new ArrayList(), new ArrayList());
//    }
//    public List<List<Integer>> combine(int n, int k, int curr, List<List<Integer>> finalList, List<Integer> list) {
//        if(k==0){
//            List<Integer> dupList= new ArrayList(list);
//            finalList.add(dupList);
//            return finalList;
//        }
//        if(k<0)
//            return finalList;
//        for(int i=curr; i<=n; i++){
//        	list.add(i);
//            finalList = combine(n, k-1, i+1, finalList, list);
//            list.remove(list.size()-1);
//        }
//        return finalList;
//    }
//    
//}
import java.util.Set;

public class Solution5 {

	public static void main(String[] args) {
		Solution6 solution = new Solution6();
		char[][] board = new char[][] { { 'O', 'O', 'O', 'O', 'X', 'X' }, { 'O', 'O', 'O', 'O', 'O', 'O' },
				{ 'O', 'X', 'O', 'X', 'O', 'O' }, { 'O', 'X', 'O', 'O', 'X', 'O' }, { 'O', 'X', 'O', 'X', 'O', 'O' },
				{ 'O', 'X', 'O', 'O', 'O', 'O' } };
		System.out.println(((1 & -1) - 1));
		Solution8 solution8 = new Solution8();
		String s = "catsanddog";
		List<String> wordDict = Arrays.asList(new String[] { "cat", "cats", "and", "sand", "dog" });
//		System.out.println(solution8.partition("aabaabccba"));
		System.out.println(solution8.isMatch("abbaabbbbababaababababbabbbaaaabbbbaaabbbabaabbbbbabbbbabbabbaaabaaaabbbbbbaaabbabbbbababbbaaabbabbabb"
										   ,  "*b*a*a*b*b*a*b*bbb*baa*bba*b*bb*b*a*aab*a*"));
//		solution.solve(board);
	}

	static int[][] direction = new int[][] { { 0, -1 }, { -1, 0 }, { 1, 0 }, { 0, 1 } };

	public static int countMatches(List<String> grid1, List<String> grid2) {
		// Write your code here
		Set<String> list1 = new HashSet<>();
		Set<String> list2 = new HashSet<>();
		if (grid1.size() == 0 || grid2.size() == 0)
			return 0;
		boolean[][] visited1 = new boolean[grid1.size()][grid1.get(0).length()];
		boolean[][] visited2 = new boolean[grid1.size()][grid1.get(0).length()];
		for (int i = 0; i < grid1.size(); i++) {
			for (int j = 0; j < grid1.get(i).length(); j++) {
				if (grid1.get(i).charAt(j) == '1' && !visited1[i][j]) {
					visited1[i][j] = true;
					list1.add(dfs(grid1, i, j, visited1, new StringBuilder("(" + i + "," + j + ")")).toString());
				}
				if (grid2.get(i).charAt(j) == '1' && !visited2[i][j]) {
					visited2[i][j] = true;
					list2.add(dfs(grid2, i, j, visited2, new StringBuilder("(" + i + "," + j + ")")).toString());
				}
			}
		}
		int count = 0;
		System.out.println(list1);
		System.out.println(list2);
		for (String s : list1) {
			if (list2.contains(s))
				count++;
		}
		return count;
	}

	public static StringBuilder dfs(List<String> grid, int row, int col, boolean[][] visited, StringBuilder sb) {
		for (int i = 0; i < 4; i++) {
			int newRow = row + direction[i][0];
			int newCol = col + direction[i][1];
			if (isValid(grid, newRow, newCol) && !visited[newRow][newCol] && grid.get(newRow).charAt(newCol) == '1') {
				sb = sb.append("(" + newRow + "," + newCol + ")");
				visited[newRow][newCol] = true;
				dfs(grid, newRow, newCol, visited, sb);
			}
		}
		return sb;
	}

	public static boolean isValid(List<String> grid, int i, int j) {
		return i >= 0 && j >= 0 && i < grid.size() && j < grid.get(0).length();
	}

}

class Solution6 {
	int[][] direction = new int[][] { { 0, 1 }, { 1, 0 }, { -1, 0 }, { 0, -1 } };

	public void solve(char[][] board) {
		boolean[][] visited = new boolean[board.length][board[0].length];
		for (int i = 1; i < board.length - 1; i++) {
			for (int j = 1; j < board[0].length - 1; j++) {
				Set<Pair> pairs = new HashSet();
				if (board[i][j] == 'O' && !visited[i][j]) {
//                	visited=new boolean[board.length][board[0].length];
					pairs.add(new Pair(i, j));
					if (dfs(board, i, j, visited, pairs)) {
						// pairs.parallelStream().forEach(pair -> board[pair.row][pair.col]='X');
						board[i][j] = 'X';
					}
				}
			}
		}
	}

	public boolean dfs(char[][] board, int row, int col, boolean[][] visited, Set<Pair> set) {
		if (board[row][col] == 'O' && (row == board.length - 1 || col == board[0].length - 1 || row == 0 || col == 0))
			return false;
		if (board[row][col] == 'X')
			return true;

		boolean flag = true;
		for (int i = 0; i < 4; i++) {
			int newRow = row + direction[i][0];
			int newCol = col + direction[i][1];
			if (row == 3) {
				System.out.println(row + " " + col + " " + newRow + " " + newCol);
			}
			if (isValid(newRow, newCol, board.length, board[0].length) && !visited[newRow][newCol]
					&& board[newRow][newCol] == 'O' && flag) {
				visited[newRow][newCol] = true;
				// set.add(new Pair(newRow, newCol));
				// board[newRow][newCol]='X';
				flag = flag && dfs(board, newRow, newCol, visited, set);
				return flag;
//                visited[newRow][newCol]=false;
			}
		}
		if (flag)
			board[row][col] = 'X';
		return flag;
	}

	public boolean isValid(int i, int j, int rows, int cols) {
		return i >= 0 && j >= 0 && i < rows && j < cols;
	}
}

class Pair {
	int row;
	int col;

	public Pair(int x, int y) {
		row = x;
		col = y;
	}
}

class Solution8 {
	int[] memo;
	List<String> list = new ArrayList();
	StringBuilder sb = new StringBuilder("");

	public List<String> wordBreak(String s, List<String> wordDict) {
		memo = new int[s.length()];
		Arrays.fill(memo, -1);
		helper(s, wordDict, 0, s.length());
		return list;
	}

	public boolean helper(String s, List<String> wordDict, int start, int end) {
		if (start == s.length()) {
			sb.setLength(sb.length() - 1);
			list.add(sb.toString());
			sb = new StringBuilder("");
			return true;
		}
		if (start > s.length()) {
			sb = new StringBuilder("");
			return false;
		}

//        if(memo[start]!=-1)
//            return memo[start]==1;
		boolean flag = false;
		for (int i = start; i <= end; i++) {
			String subStr = s.substring(start, i);
			if (subStr.equals("cats"))
				System.out.println(wordDict.contains(subStr) + ":" + sb);
			if (wordDict.contains(subStr)) {
				sb.append(subStr + " ");
				System.out.println(subStr.equals("cats") + ":" + sb + ":" + i);
				flag = helper(s, wordDict, i, end) || flag;
			}
		}
		memo[start] = (flag == true ? 1 : 0);
		return flag;
	}

	public List<List<String>> partition(String s) {
		return partition(s, 0, s.length(), new ArrayList<>(), new ArrayList<>(), new StringBuilder());
	}

	public List<List<String>> partition(String s, int start, int end, List<List<String>> finAlList,
			List<String> strList, StringBuilder sb) {
		if (start > s.length())
			return finAlList;
		if (start == s.length()) {
			List<String> list = new ArrayList<>(strList);
			finAlList.add(list);
			strList = new ArrayList<>();
			return finAlList;
		}
		sb = new StringBuilder();
		for (int i = start; i < end; i++) {
			sb = new StringBuilder(s.substring(start, i + 1));
			if (isPalindrome(sb)) {
				strList.add(sb.toString());
				finAlList = partition(s, i + 1, end, finAlList, strList, sb);
				strList.remove(strList.size() - 1);
			}
		}
		return finAlList;
	}

	public boolean isPalindrome(StringBuilder a) {
		return a.toString().equals(a.reverse().toString());
	}

	public boolean isMatch(String s, String p) {
		cache=new int[s.length()+1][p.length()+1];
		cache[s.length()][p.length()]=1;
		for(int[] arr: cache)
			Arrays.fill(arr, -1);
		return isMatch(s, p, 0, 0);
	}
	int[][] cache;
	public boolean isMatch(String s, String p, int sIndex, int pIndex) {

		boolean flag = false;
		if (p.length() == pIndex && sIndex == s.length()) {
			return true;
		}
		else if(cache[sIndex][pIndex]!=-1) {
			return cache[sIndex][pIndex]==1;
		}
		else if ((sIndex < s.length() && pIndex >= p.length()))
			flag= false;
		else if ((sIndex >= s.length() && pIndex < p.length() && p.charAt(pIndex) != '*' && pIndex != p.length() - 1))
			flag= false;

		else if (p.charAt(pIndex) == '*' && s.length() == sIndex && pIndex == p.length() - 1)
			flag= true;
		else if (pIndex == p.length() || sIndex == s.length())
			flag= false;

		else if (p.charAt(pIndex) == '?') {
			if (pIndex < p.length() - 1 && sIndex < s.length() - 1 && p.charAt(pIndex + 1) != '*'
					&& p.charAt(pIndex + 1) != '?')
				flag= (s.charAt(sIndex + 1) == p.charAt(pIndex + 1)) && isMatch(s, p, sIndex + 2, pIndex + 2) || flag;
			else
				flag= isMatch(s, p, sIndex + 1, pIndex + 1) || flag;

		}

		else if (s.charAt(sIndex) == p.charAt(pIndex)) {
			flag= isMatch(s, p, sIndex + 1, pIndex + 1) || flag;
		} else if (p.charAt(pIndex) == '*') {
			flag = isMatch(s, p, sIndex+1, pIndex) || isMatch(s, p, sIndex, pIndex+1);
//			int index = pIndex;
//			if (index + 1 < p.length()) {
//				for (int i = sIndex; i < s.length(); i++) {
//					if (p.charAt(pIndex + 1) == s.charAt(i) || p.charAt(pIndex + 1) == '?')
//						flag = isMatch(s, p, i + 1, pIndex + 2) || flag;
//				}
//			} else
//				flag= true;
//			return flag;
		}
		cache[sIndex][pIndex]=flag?1:0;
		return flag;
	}
}
