package com.zolve.wallet;

import java.util.Arrays;

public class Solution4 {
	int[][] dir = new int[][] { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };
	int result = 1;
	int[][] dist;

	public static void main(String[] args) {
		int[][] matrix = new int[][] { { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 19, 18, 17, 16, 15, 14, 13, 12, 11, 10 },
				{ 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 }, { 39, 38, 37, 36, 35, 34, 33, 32, 31, 30 },
				{ 40, 41, 42, 43, 44, 45, 46, 47, 48, 49 }, { 59, 58, 57, 56, 55, 54, 53, 52, 51, 50 },
				{ 60, 61, 62, 63, 64, 65, 66, 67, 68, 69 }, { 79, 78, 77, 76, 75, 74, 73, 72, 71, 70 },
				{ 80, 81, 82, 83, 84, 85, 86, 87, 88, 89 }, { 99, 98, 97, 96, 95, 94, 93, 92, 91, 90 },
				{ 100, 101, 102, 103, 104, 105, 106, 107, 108, 109 },
				{ 119, 118, 117, 116, 115, 114, 113, 112, 111, 110 },
				{ 120, 121, 122, 123, 124, 125, 126, 127, 128, 129 },
				{ 139, 138, 137, 136, 135, 134, 133, 132, 131, 130 }, { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } };

		System.out.println(new Solution4().longestIncreasingPath(matrix));
	}

	public int longestIncreasingPath(int[][] matrix) {
		int max = 0;
		if (matrix.length == 0)
			return 0;
		if (matrix.length == 1 && matrix[0].length == 1)
			return 1;
		dist = new int[matrix.length][matrix[0].length];
		for (int[] row : dist)
			Arrays.fill(row, Integer.MIN_VALUE);
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {

				if (dist[i][j] == Integer.MIN_VALUE)
					dist[i][j] = 1;
				int solution = longestIncreasingPath(matrix, i, j);
				if (solution > max)
					max = solution;
			}
		}
		return max;
	}

	public int longestIncreasingPath(int[][] matrix, int row, int col) {
		for (int i = 0; i < 4; i++) {
			int newRow = row + dir[i][0];
			int newCol = col + dir[i][1];
			if (isValid(newRow, newCol, matrix.length, matrix[0].length) && matrix[newRow][newCol] > matrix[row][col]
					&& dist[newRow][newCol] <= dist[row][col]) {
				dist[newRow][newCol] = dist[row][col] + 1;
				longestIncreasingPath(matrix, newRow, newCol);
				result = Math.max(result, dist[newRow][newCol]);
			}
		}
		return result;
	}

	public boolean isValid(int i, int j, int rows, int cols) {
		return i >= 0 && j >= 0 && i < rows && j < cols;
	}
}
