package com.zolve.wallet;

import java.util.HashMap;
import java.util.Map;

public class DesignO1 {

	public static void main(String[] args) {
		DesignO1 sol = new DesignO1(10);
		sol.add(10);
		sol.add(21);
		sol.add(35);
		sol.add(48);
		sol.add(53);
		sol.add(67);
		System.out.println(sol.search(10));
		sol.remove(10);
		System.out.println(sol.search(67));
		System.out.println(sol.getRandom());

	}

	Map<Integer, Integer> map = new HashMap();
	Integer[] array;
	int lastIndex = -1;

	public DesignO1(int N) {
		array = new Integer[N];
	}

	public void add(int val) {
		map.put(val, ++lastIndex);
		array[lastIndex] = val;
	}

	public int search(int val) {
		Integer index = map.get(val);
		if (index != null)
			return index;
		return -1;
	}

	public void remove(int val) {
		int index = search(val);
		if (index == lastIndex) {
			array[lastIndex--] = null;
			map.remove(val);
		} else if (index != -1) {
			map.remove(val);
			int temp = array[lastIndex];
			array[index] = temp;
			map.put(temp, index);
			array[lastIndex--] = null;
		}
	}

	public int getRandom() {
		int ran = (int) (Math.random() * lastIndex);
		return array[ran];
	}

	public void swap(int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}
