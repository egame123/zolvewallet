package com.zolve.wallet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

class ListNode {
	int val;
	ListNode next;

	ListNode() {
	}

	ListNode(int val) {
		this.val = val;
	}

	ListNode(int val, ListNode next) {
		this.val = val;
		this.next = next;
	}
}

class Solution {
	public ListNode swap(ListNode head) {
		if (head == null || head.next == null)
			return head;
		ListNode temp = head.next;
		head.next = head.next.next;
		temp.next = head;
		head = temp;
		return head;
	}
	public ListNode swapPairs(ListNode head) {
		if (head == null || head.next == null)
			return head;
		ListNode temp = null;/*
							 * .next; head.next=head.next.next; temp.next=head; head=temp
							 */;
		ListNode node=null;
		while(head!=null && head.next!=null) {
			head=swap(head);
			if(temp==null) {
				temp=head;
				node=temp;
			}
			else {
				temp.next.next=head;
				temp=temp.next.next;
			}
			head=head.next.next;
		}
		return node;
	}

	int[][] direction = new int[][] { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 } };
	boolean[][] visited;

	int[][] dist;

	public boolean isValidMove(int i, int j, int rows, int cols) {
		return (i >= 0 && j >= 0 && i < rows && j < cols);
	}

	public int findHayStick(String text, String pattern) {
		int[] lps = lps(pattern);
		int i = 0;
		int j = 0;
		while (i < text.length()) {
			if (text.charAt(i) == pattern.charAt(j)) {
				i++;
				j++;
			}
			if (j == pattern.length() - 1)
				return i - j;
			if (i < text.length() && pattern.charAt(j) != text.charAt(i)) {
				if (j != 0)
					j = lps[j - 1];
				else
					i += 1;
			}

		}
		return -1;
	}

	public int[] lps(String pattern) {
		int[] lps = new int[pattern.length()];
		int i = 1;
		int len = 0;
		int M = pattern.length();
		while (i < M) {
			if (pattern.charAt(i) == pattern.charAt(len)) {
				lps[i++] = ++len;

			} else {
				if (len != 0) {
					len = lps[len - 1];
				} else {
					lps[i] = len;
					i++;
				}
			}
		}
		return lps;
	}

	public static void main(String[] args) {

		Solution solution = new Solution();
//[[1,3],[3,0,1],[2],[0]]
		List<List<Integer>> rooms = new ArrayList<>();
		System.out.println((8 >> 33));
		rooms.add(Arrays.asList(3, 1));
		rooms.add(Arrays.asList(3, 0, 1));
		rooms.add(Arrays.asList(2));
		rooms.add(Arrays.asList(0));
//		System.out.println(solution.findHayStick("Hello", "ll"));
		ListNode head=new ListNode(1);
		head.next=new ListNode(2);
		head.next.next=new ListNode(3);
		head.next.next.next=new ListNode(4);
		head.next.next.next.next=new ListNode(5);
		head.next.next.next.next.next=new ListNode(6);
		head=solution.swapPairs(head);
		
	}

	public boolean canVisitAllRooms(List<List<Integer>> rooms) {

		boolean[] visited = new boolean[rooms.size()];
		visited = dfs(0, rooms, visited);
		for (int i = 0; i < visited.length; i++) {
			if (!visited[i])
				return false;
		}
		return true;
	}

	public boolean[] dfs(int key, List<List<Integer>> rooms, boolean[] visited) {
		List<Integer> keys = rooms.get(key);
		visited[key] = true;

		for (int newKey : keys) {
			if (!visited[newKey]) {
				dfs(newKey, rooms, visited);
			}
		}
		return visited;
	}

	public int[][] updateMatrix(int[][] matrix) {
		int rows = matrix.length;
		int cols = matrix[0].length;
		visited = new boolean[rows][cols];
		dist = new int[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				if (matrix[i][j] == 0)
					dist[i][j] = matrix[i][j];
				else
					dist[i][j] = Integer.MAX_VALUE;
			}

		}
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (matrix[i][j] == 0)
					bfs(matrix, i, j, rows, cols);
			}
		}
		return dist;
	}

	public int[][] bfs(int[][] matrix, int row, int col, int rows, int cols) {
		Queue<List<Integer>> queue = new LinkedList<>();
		queue.add(Arrays.asList(row, col));
//		queue.p
//		dist[row][col] = 0;
		while (!queue.isEmpty()) {
			List<Integer> ele = queue.poll();
			for (int i = 0; i < 4; i++) {
				int newRow = ele.get(0) + direction[i][0];
				int newCol = ele.get(1) + direction[i][1];
				if (isValidMove(newRow, newCol, rows, cols)
						&& dist[newRow][newCol] > 1 + dist[ele.get(0)][ele.get(1)]) {
					queue.add(Arrays.asList(newRow, newCol));
					dist[newRow][newCol] = 1 + dist[ele.get(0)][ele.get(1)];
				}
			}
		}
		return dist;
	}

	public int[][] dfs(int[][] matrix, int row, int col) {

		if (matrix[row][col] == 0) {
			visited[row][col] = true;
			return matrix;
		} else {
			int min = matrix[row][col];
			for (int i = 0; i < 4; i++) {
				int newRow = row + direction[i][0];
				int newCol = col + direction[i][1];
				if (isValidMove(newRow, newCol, matrix.length, matrix[0].length)) {
					if (!visited[newRow][newCol]) {
						visited[newRow][newCol] = true;
						matrix = dfs(matrix, newRow, newCol);
						visited[newRow][newCol] = false;
					}
					if (matrix[newRow][newCol] < min)
						min = matrix[newRow][newCol];
				}
			}
			visited[row][col] = true;
			matrix[row][col] = 1 + min;
		}
		return matrix;
	}

	public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
		int rows = image.length;
		int column = image[0].length;

		int color = image[sr][sc];
		Queue<List<Integer>> queue = new LinkedList<>();
		boolean[][] visited = new boolean[rows][column];
		if (isValidMove(sr, sc, rows, column))
			queue.add(Arrays.asList(sr, sc));
		while (!queue.isEmpty()) {
			List<Integer> ele = queue.poll();
			image[ele.get(0)][ele.get(1)] = newColor;
			for (int i = 0; i < 4; i++) {
				int newRow = ele.get(0) + direction[i][0];
				int newCol = ele.get(1) + direction[i][1];
				if (isValidMove(newRow, newCol, rows, column) && !visited[newRow][newCol]
						&& image[newRow][newCol] == color) {
					queue.add(Arrays.asList(newRow, newCol));
					visited[newRow][newCol] = true;
				}
			}
		}
		return image;
	}

	/*
	 * public int[][] dfs(int[][] image, int row, int col, int newColor, boolean[][]
	 * visited, int color){ visited[row][col]=true; if(image[row][col]==color)
	 * image[row][col]=newColor;
	 * 
	 * for(int i=0; i<4; i++) { int newRow = row+direction[i][0]; int newCol =
	 * col+direction[i][1]; if(isValidMove(newRow, newCol, image.length,
	 * image[0].length) && !visited[newRow][newCol] && image[newRow][newCol]==color)
	 * { dfs(image, newRow, newCol, newColor, visited, color); } } return image; }
	 */

	public int findTargetSumWays(int[] nums, int S, int start, int ways, int len, int[][] memo) {
		if (len == nums.length) {
			if (S == 0)
				return 1;
			return 0;
		}
		if (memo[start][S + 1000] != Integer.MIN_VALUE)
			return memo[start][S + 1000];
		int waysL = findTargetSumWays(nums, S - nums[start], start + 1, ways, len + 1, memo);
		int waysR = findTargetSumWays(nums, S + nums[start], start + 1, ways, len + 1, memo);
		memo[start][S + 1000] = waysL + waysR;
		if (memo[start][S + 1000] == Integer.MIN_VALUE)
			return 0;
		return memo[start][S + 1000];
	}

	public String decodeString(String s) {
		if (s == null || s == "")
			return s;
		Stack<String> stack = new Stack<>();
		char[] chars = s.toCharArray();

		int i = 0;
		StringBuilder result = new StringBuilder();
		String numstr = "";

		stack.push(numstr);
		while (i < s.length()) {
			if (Character.isDigit(chars[i])) {
				while (Character.isDigit(chars[i])) {
					numstr += chars[i];
					i++;
				}

			} else if (chars[i] == ']') {// 3[a2[c]]
				StringBuilder sb = new StringBuilder();
				while (!stack.isEmpty() && !"[".equals(stack.peek())) {
					sb.append(stack.pop());
				}
				String torepeat = sb.reverse().toString();
				stack.pop();
				int num = Integer.valueOf(stack.pop());
				String sub = "";
				for (int j = 0; j < num; j++)
					sub += torepeat;
				stack.push(sub);
			} else
				stack.push(chars[i] + "");
			i++;

		} /*
			 * Stack st = new Stack<>(); while (!stack.isEmpty()) { st.push(stack.pop()); }
			 * while (!st.isEmpty()) result.append(st.pop());
			 */
		return result.toString();
	}

}
/*
 * class Solution {
 * 
 * int[][] direction = new int[][] { { 1, 0 }, { 0, 1 }, { -1, 0 }, { 0, -1 } };
 * 
 * 
 * 
 * 
 * public int numIslands(char[][] grid) { int m = grid.length; int n =
 * grid[0].length; int numIslands = 0; boolean[][] visited = new boolean[m][n];
 * for (int i = 0; i < m; i++) { for (int j = 0; j < n; j++) { if (grid[i][j] ==
 * '1' && !visited[i][j]) { numIslands++; visited[i][j]=true; visited =
 * dfs(grid, i, j, visited); } } } return numIslands; }
 * 
 * public boolean[][] dfs(char[][] grid, int i, int j, boolean[][] visited) {
 * for (int k = 0; k < 4; k++) { int newRow = i + direction[k][0]; int newCol =
 * j + direction[k][1]; if (isValidMove(newRow, newCol, grid.length,
 * grid[0].length) && !visited[newRow][newCol] && grid[newRow][newCol] == '1') {
 * visited[newRow][newCol] = true; dfs(grid, newRow, newCol, visited); } }
 * return visited; }
 * 
 * public boolean isValidMove(int i, int j, int rows, int cols) { return (i >= 0
 * && j >= 0 && i < rows && j < cols); }
 * 
 * 
 * 
 * public int openLock(String[] deadends, String target) { Queue<String> queue =
 * new LinkedList<>(); queue.add("0000"); int steps = 0; HashSet<String> visited
 * = new HashSet<>(); HashSet<String> deadSet = new HashSet<>();
 * deadSet.addAll(Arrays.asList(deadends)); if(deadSet.contains("0000")) return
 * -1; while (!queue.isEmpty()) { int size = queue.size(); steps++; for (int i =
 * 0; i < size; i++) { String value = queue.poll(); visited.add(value); if
 * (value.equals(target)) return steps-1; List<String> newValues =
 * allnewPossibilities(value, visited, deadSet); queue.addAll(newValues); } }
 * return -1; }
 * 
 * private List<String> allnewPossibilities(String value, HashSet<String>
 * visited, HashSet<String> deadSet) { List<String> allNewPossibilities = new
 * ArrayList<>(); for (int i = 0; i < 4; i++) { char num = value.charAt(i);
 * char[] valueArray = value.toCharArray(); if (num == '0') num = '9'; else
 * num--; valueArray[i] = (char) num; String newNegValue = new
 * String(valueArray); if (!deadSet.contains(newNegValue) &&
 * visited.add(newNegValue)) allNewPossibilities.add(newNegValue); num =
 * value.charAt(i); valueArray = value.toCharArray(); if (num == '9') num = '0';
 * else num++; valueArray[i] = (char) num; String newPosValue = new
 * String(valueArray); if (!deadSet.contains(newPosValue) &&
 * visited.add(newPosValue)) allNewPossibilities.add(newPosValue);
 * 
 * } return allNewPossibilities; } }
 */