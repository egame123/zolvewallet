package com.zolve.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zolve.dto.Response;
import com.zolve.entity.User;
import com.zolve.service.UserService;

@RestController
public class UserController {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private UserService userService;
	
	@PostMapping(value = "/saveUser")
	public ResponseEntity<Response<User>> saveUser(@RequestBody User user) {
		Response<User> response= new Response<>();
		try{
			User savedUser = userService.save(user);
			response.setData(savedUser);
			response.setMessage("Successfully Saved User");
			response.setStatus(200);
			return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
		}catch (Exception e) {
			logger.error(e.getMessage());
			response.setMessage(e.getMessage());
			response.setStatus(400);
			return new ResponseEntity<Response<User>>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/getUserById")
	public ResponseEntity<Response<User>> getUserById(@RequestParam("phone") String phone) {
		Response<User> response= new Response<>();
		try {
			 User user = userService.findUserById(phone);
			 if(user==null)
				 throw new Exception("User with id: "+phone+" doesn't exist");
			 response.setData(user);
			 response.setMessage("Success");
			 response.setStatus(200);
			 return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setStatus(400);
			e.printStackTrace();
			return new ResponseEntity<Response<User>>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
}
