package com.zolve.wallet;

class Node {
	int val;
	Node next;

	Node() {
	}

	Node(int val) {
		this.val = val;
	}

	Node(int val, Node next) {
		this.val = val;
		this.next = next;
	}
}

public class Solution3 {
	public static void main(String[] args) {
		Node head1=new Node(1);
		head1.next=new Node(2);
		head1.next.next=new Node(4);
		

		Node head=new Node(1);
		head.next=new Node(2);
		head.next.next=new Node(3);
		Node lists = new Solution3().mergeTwoLists(head1, head);
	}

	public Node mergeTwoLists(Node l1, Node l2) {
		if (l1 == null)
			return l2;
		if (l2 == null)
			return l1;
		Node head = l1.val <= l2.val ? l1 : l2;
		while (l1 != null && l2 != null) {
			System.out.println(l1.val + " " + l2.val);
			while (l1 != null && l1.next != null && l2 != null && l1.val < l2.val) {
				l1 = l1.next;
			}
			while (l1 != null && l2.next!=null && l2 != null && l2.val < l1.val) {
				l2 = l2.next;
			}
			if (l1 != null && l2 != null && l1.val <= l2.val) {
				Node temp = l1.next;
				l1.next = l2;
				l2 = temp;
			} else if (l1 != null && l2 != null && l2.val < l1.val) {
				Node temp = l2.next;
				l2.next = l1;
				l1 = temp;
			}
		}
		return head;
	}
}