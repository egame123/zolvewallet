package com.zolve.wallet;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class MangoRott {
	int[][] direction = new int[][] { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };
	int[][] dist;

	public static void main(String[] args) {
		int[][] board = new int[][] { { 2, 1, 0, 2, 1 }, 
									  { 1, 0, 1, 1, 1 }, 
									  { 0, 0, 0, 2, 1 } };
		 System.out.println(new MangoRott().calcTime(board));
	}

	public int calcTime(int[][] board) {
		dist = new int[board.length][board[0].length];
		for (int[] arr : dist) {
			Arrays.fill(arr, Integer.MAX_VALUE);
		}
		Queue<List<Integer>> queue = new LinkedList<>();
		for(int i=0; i<board.length; i++) {
			for(int j=0; j<board[0].length; j++) {
				if(board[i][j]==2) {
					dist[i][j]=0;
					queue.add(Arrays.asList(i, j));
				}
			}
		}

		bfs(board, queue);
		int max=-1;
		for(int i=0; i<board.length; i++) {
			for(int j=0; j<board[0].length; j++) {
				if(dist[i][j]!=Integer.MAX_VALUE && dist[i][j]>max)
					max=dist[i][j];
				else if(board[i][j]==1)
					return -1;
			}
		}
		return max;
	}
	public int dfs(int[][] board, int row, int col, boolean[][] visited) {
			for (int i = 0; i < 4; i++) {
				int newRow = row + direction[i][0];
				int newCol = col+direction[i][1];
				if (isValid(newRow, newCol, board.length, board[0].length) && board[newRow][newCol] == 1
						&& !visited[newRow][newCol] && dist[newRow][newCol] > dist[row][col] + 1) {
					visited[newRow][newCol] = true;
					dist[newRow][newCol] = dist[row][col] + 1;
					board[newRow][newCol]=2;
					dfs(board, newRow, newCol, visited);
					visited[newRow][newCol]=false;
					board[newRow][newCol]=1;
				}
		}

		return -1;
	}
	public int[][] bfs(int[][] board, Queue<List<Integer>> queue) {
		while (!queue.isEmpty()) {
			List<Integer> ele = queue.poll();
			for (int i = 0; i < 4; i++) {
				int newRow = direction[i][0] + ele.get(0);
				int newCol = direction[i][1] + ele.get(1);
				if (isValid(newRow, newCol, board.length, board[0].length) && board[newRow][newCol] == 1
						&& dist[newRow][newCol] > dist[ele.get(0)][ele.get(1)] + 1) {
					dist[newRow][newCol] = dist[ele.get(0)][ele.get(1)] + 1;
					board[newRow][newCol]=2;
					queue.add(Arrays.asList(newRow, newCol));
				}
			}
		}
		return board;
	}

	public boolean isValid(int i, int j, int rows, int cols) {
		return i >= 0 && j >= 0 && i < rows && j < cols;
	}
}
