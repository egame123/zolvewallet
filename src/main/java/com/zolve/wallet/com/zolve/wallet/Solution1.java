package com.zolve.wallet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Solution1 {
	public static void main(String[] args) {
		char[][] board= {{'o','a','a','n'},
						 {'e','t','a','e'},
						 {'i','h','k','r'},
						 {'i','f','l','v'}};
		String[] words={"hklf", "hf"};
		Solution1 solution1=new Solution1();
		solution1.findWords(board, words);
	}
    public List<String> findWords(char[][] board, String[] words) {
        Trie trie=new Trie();
        for(String word: words){
            trie.add(word);
        }
        List<String> list=new ArrayList();
        
        for(int i=0; i<board.length; i++){
            for(int j=0; j<board[0].length; j++){
                boolean[][] visited=new boolean[board.length][board[0].length];
                visited[i][j]=true;
                if(trie.root.children.get(board[i][j])!=null)
                    list= trie.search(new StringBuilder(), board, list, 
                                      i, j, trie.root.children.get(board[i][j]), visited);
            }
        }
        return list;
    }
}

class Trie{
    int[][] direction=new int[][]{{1,0}, {0,1},{-1,0}, {0,-1}};
    TrieNode root=new TrieNode();
    public void add(String word){
        TrieNode head=root;
        for(int i=0; i<word.length(); i++){
            if(head.children.get(word.charAt(i))==null)
                head.children.put(word.charAt(i), new TrieNode());
            head=head.children.get(word.charAt(i));
        }
        head.end=true;
    }
    
    public List<String> search(StringBuilder word, char[][] board, List<String> list, 
                               int row, int col, TrieNode head, boolean[][] visited){
        TrieNode prev=head;
        word.append(board[row][col]);
        if(head.end)
            list.add(new String(word.toString()));
//        if(head.children.get(board[row][col])==null)
//            return list;
        for(int i=0; i<4; i++){
            int newRow=row+direction[i][0];
            int newCol=col+direction[i][1];
            if(isValidMove(newRow, newCol, board.length, board[0].length) && !visited[newRow][newCol] 
              && head.children.get(board[newRow][newCol])!=null){
                System.out.println(head.children.keySet());
            	
                head=head.children.get(board[newRow][newCol]);
//                word.append(board[newRow][newCol]);
                visited[newRow][newCol]=true;
                search(word, board, list, newRow, newCol, head, visited);
                head=prev;
                visited[newRow][newCol]=false;
                // head=prev;
                word.setLength(word.length() - 1);
            }
        }
        return list;
    }
    
    public boolean isValidMove(int i, int j, int rows, int cols) {
		return (i >= 0 && j >= 0 && i < rows && j < cols);
	}
}

class TrieNode{
    Map<Character, TrieNode> children=new HashMap();
    boolean end=false;
}
