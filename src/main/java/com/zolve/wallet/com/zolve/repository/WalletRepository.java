package com.zolve.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import com.zolve.entity.Wallet;


@Repository
public class WalletRepository {
	private final MongoTemplate mongoTemplate;
	
    @Autowired
    public WalletRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
    
    public Wallet insert(Wallet wallet) {
    	return mongoTemplate.insert(wallet);
	}
    
    public Wallet save(Wallet wallet) {
    	return mongoTemplate.save(wallet);
	}
    
    public Wallet findWalletById(String id) {
    	return mongoTemplate.findById(id, Wallet.class);
    }
    
    public List<Wallet> getAllWallets(){
    	Query query = new Query();
		Criteria criteria = Criteria.where("transactionType").is("");
		query.addCriteria(criteria);
    	return mongoTemplate.find(query, Wallet.class);
    }

	public Wallet findWalletByUserId(String userId) {
		Query query = new Query();
		Criteria criteria = Criteria.where("userId").is(userId);
		query.addCriteria(criteria);
		return mongoTemplate.findOne(query, Wallet.class);
	}
	
	public List<Wallet> getAllCreditStatements(String userId) {
		Query query = new Query();
		Criteria criteria = Criteria.where("userId").is(userId);
		query.addCriteria(Criteria.where("transactionType").is("CREDITED"));
		query.addCriteria(criteria);
		query.with(Sort.by(Sort.Direction.DESC, "updatedAt"));
		return mongoTemplate.find(query, Wallet.class);
	}
	
	public List<Wallet> getAllDebitStatements(String userId) {
		Query query = new Query();
		Criteria criteria = Criteria.where("userId").is(userId);
		query.addCriteria(Criteria.where("transactionType").is("DEBITED"));
		query.addCriteria(criteria);
		query.with(Sort.by(Sort.Direction.DESC, "updatedAt"));
		System.out.println(query);
		return mongoTemplate.find(query, Wallet.class);
	}
	
	public List<Wallet> getAllStatements(String userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userId").is(userId).orOperator(
							Criteria.where("transactionType").is("CREDITED"),
							Criteria.where("transactionType").is("DEBITED"))
						);
		query.with(Sort.by(Sort.Direction.DESC, "updatedAt"));
		System.out.println(query);
		return mongoTemplate.find(query, Wallet.class);
	}

	public Wallet getLastWalletTransaction(String userId) {
		Query query = new Query();
		Criteria criteria = Criteria.where("userId").is(userId);
		query.addCriteria(criteria);
		query.limit(1);
		query.with(Sort.by(Sort.Direction.DESC, "updatedAt"));
		List<Wallet> lastTransaction = mongoTemplate.find(query, Wallet.class);
		if(lastTransaction!=null && lastTransaction.size()>0)
			return lastTransaction.get(0);
		return null;
	}
}
