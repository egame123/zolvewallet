package com.zolve.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.zolve.dto.WalletDTO;
import com.zolve.entity.User;
import com.zolve.entity.Wallet;
import com.zolve.repository.WalletRepository;

@Service
public class WalletService {
	@Autowired
	private WalletRepository walletRepository;
	@Autowired
	private UserService userService;

	@Autowired
	private ReentrantLock reentrantLock;

	@Value("${wallet.minBalance}")
	private double minBalance;

	public Wallet createWallet(String userId) throws Exception {
		if (userId == null || "".equals(userId))
			throw new Exception("userdId can't be null or empty!!!");

		Wallet wallet = new Wallet();
		wallet.setUserId(userId);
		wallet.setUpdatedAt(LocalDateTime.now());
		wallet.setTransactionType("");
		User user = userService.findUserById(wallet.getUserId());
		if (user == null)
			throw new Exception("User Doesn't Exist");
		if (walletRepository.findWalletByUserId(userId) != null)
			throw new Exception("Wallet already exists");

		return walletRepository.insert(wallet);
	}

	public Wallet findWalletById(String id) throws Exception {
		if (id == null)
			throw new Exception("Wallet Id can't be null");
		return walletRepository.findWalletById(id);
	}

	public List<Wallet> getAllWallets() {
		return walletRepository.getAllWallets();
	}

	public Wallet convertWalletDtoToWallet(WalletDTO walletDTO) {
		Wallet wallet = new Wallet();
		if (walletDTO == null)
			return null;
		wallet.setBalance(walletDTO.getBalance());
		wallet.setUserId(walletDTO.getUserId());
		return wallet;
	}

	public Wallet creditMoney(String userId, double amount) throws Exception {
		try {
			if (userId == null || userService.findUserById(userId) == null)
				throw new Exception("User Doesn't Exist");

			if (amount <= 0)
				throw new Exception("amount credited should be greater than 0");
			reentrantLock.tryLock(2000, TimeUnit.MILLISECONDS);
			Wallet savedWallet = walletRepository.getLastWalletTransaction(userId);
			double balance = 0;
			Wallet wallet = new Wallet();
			wallet.setUserId(userId);
			wallet.setAmount(amount);
			if (savedWallet != null) {
				balance = savedWallet.getBalance();
			}
			wallet.setTransactionType("CREDITED");
			wallet.setUpdatedAt(LocalDateTime.now());
			wallet.setBalance(balance + amount);
			return walletRepository.save(wallet);
		} finally {
			reentrantLock.unlock();
		}
	}

	public Wallet debitMoney(String userId, double amount) throws Exception {
		try {
			if (userId == null || userService.findUserById(userId) == null)
				throw new Exception("User Doesn't Exist");

			if (amount <= 0)
				throw new Exception("amount credited should be greater than 0");
			reentrantLock.tryLock(2000, TimeUnit.MILLISECONDS);
			Wallet lastTransaction = walletRepository.getLastWalletTransaction(userId);
			double balance = 0;
			Wallet wallet = new Wallet();
			wallet.setUserId(userId);
			wallet.setAmount(amount);
			if (lastTransaction != null) {
				balance = lastTransaction.getBalance();
			}
			if (balance - amount < minBalance)
				throw new Exception("Insufficient balance " + balance + ", please recharge");

			wallet.setTransactionType("DEBITED");
			wallet.setUpdatedAt(LocalDateTime.now());
			wallet.setBalance(balance - amount);
			return walletRepository.save(wallet);
		} finally {
			reentrantLock.unlock();
		}
	}

	public Wallet findWalletByUserId(String userId) {

		return walletRepository.findWalletByUserId(userId);
	}

	public double getCurrentBalance(String userId) throws Exception {
		Wallet wallet = null;
		try {
			reentrantLock.tryLock();
			wallet = walletRepository.getLastWalletTransaction(userId);
		} finally {
			reentrantLock.unlock();
		}
		if (wallet == null)
			throw new Exception("wallet doesn't exist");
		return wallet.getBalance();
	}

	public List<Wallet> getCreditLogs(String userId) throws Exception {
		if (userId == null || userId.equals(""))
			throw new Exception("Invalid UserId");
		User user = userService.findUserById(userId);
		if (user == null)
			throw new Exception("User Doesnt Exist");
		if (walletRepository.findWalletByUserId(userId) == null)
			throw new Exception("Wallet for user: " + userId + " doesn't exist");
		List<Wallet> creditStatements = walletRepository.getAllCreditStatements(userId);
		if (creditStatements == null || creditStatements.isEmpty())
			throw new Exception("No Credit Statement Found");

		return creditStatements;
	}

	public List<Wallet> getDebitLogs(String userId) throws Exception {
		if (userId == null || userId.equals(""))
			throw new Exception("Invalid UserId");
		User user = userService.findUserById(userId);
		if (user == null)
			throw new Exception("User Doesnt Exist");
		if (walletRepository.findWalletByUserId(userId) == null)
			throw new Exception("Wallet for user: " + userId + " doesn't exist");
		List<Wallet> debitStatements = walletRepository.getAllDebitStatements(userId);
		if (debitStatements == null || debitStatements.isEmpty())
			throw new Exception("No Credit Statement Found");

		return debitStatements;
	}

	public List<Wallet> getAllStatements(String userId) throws Exception {
		if (userId == null || userId.equals(""))
			throw new Exception("Invalid UserId");
		User user = userService.findUserById(userId);
		if (user == null)
			throw new Exception("User Doesnt Exist");
		if (walletRepository.findWalletByUserId(userId) == null)
			throw new Exception("Wallet for user: " + userId + " doesn't exist");
		List<Wallet> allStatement = walletRepository.getAllStatements(userId);
		if (allStatement == null || allStatement.isEmpty())
			throw new Exception("No Credit Statement Found");

		return allStatement;
	}
}
