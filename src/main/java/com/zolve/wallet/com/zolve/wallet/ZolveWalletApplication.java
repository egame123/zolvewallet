package com.zolve.wallet;

import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication(scanBasePackages = "com.zolve")
@EnableMongoAuditing 
public class ZolveWalletApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZolveWalletApplication.class, args);
		Scanner sc = new Scanner(System.in);
		for(;;) {
			System.out.println("Enter something:");
			String input =sc.next();
			System.err.println("You entered: "+input);
		}
	}

	@Bean
	public ReentrantLock lock() {
		return new ReentrantLock(true);
	}
}
