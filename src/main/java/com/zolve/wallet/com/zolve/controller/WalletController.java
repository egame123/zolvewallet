package com.zolve.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.zolve.dto.Response;
import com.zolve.entity.Wallet;
import com.zolve.service.WalletService;

@RestController
public class WalletController {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private WalletService walletService;
	
	@PostMapping(value = "/saveWallet")
	public ResponseEntity<Response<Wallet>> saveWallet(@RequestParam("userId") String userId) {
		Response<Wallet> response = new Response<>();
		try {
			Wallet wallet = walletService.createWallet(userId);
			response.setData(wallet);
			response.setMessage("Successfully created wallet");
			response.setStatus(200);
			return new ResponseEntity<Response<Wallet>>(response, HttpStatus.OK);
		}catch (Exception e) {
			logger.error(e.getMessage());
			response.setMessage(e.getMessage());
			response.setStatus(400);
			return new ResponseEntity<Response<Wallet>>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/getWalletById")
	public ResponseEntity<Response<Wallet>> getWalletById(@RequestParam("userId") String userId) {
		Response<Wallet> response = new Response<>();
		try {
			Wallet wallet = walletService.findWalletByUserId(userId);
			if(wallet==null)
				throw new Exception("Wallet doesn't Exist");
			response.setData(wallet);
			response.setMessage("Successfully retreived wallet details");
			response.setStatus(200);
			return new ResponseEntity<Response<Wallet>>(response, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setMessage(e.getMessage());
			response.setStatus(400);
			return new ResponseEntity<Response<Wallet>>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping(value = "/getAllWallets")
	public ResponseEntity<Response<List<Wallet>>> getAllWallets() {
		Response<List<Wallet>> response = new Response<>();
		try {
			List<Wallet> wallets = walletService.getAllWallets();
			response.setData(wallets);
			response.setMessage("Successfully retreived wallets");
			response.setStatus(200);
			return new ResponseEntity<Response<List<Wallet>>>(response, HttpStatus.OK);
		}catch (Exception e) {
			logger.error(e.getMessage());
			response.setMessage(e.getMessage());
			response.setStatus(400);
			return new ResponseEntity<Response<List<Wallet>>>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/credWallet")
	public ResponseEntity<Response<Wallet>> credWallet(@RequestParam("userId") String userId, @RequestParam("amount") double amount) {
		Response<Wallet> response = new Response<>();
		try {
			Wallet creditedWallet = walletService.creditMoney(userId, amount);
			response.setMessage("Successfully credited the wallet");
			response.setData(creditedWallet);
			response.setStatus(200);
			return new ResponseEntity<Response<Wallet>>(response, HttpStatus.OK);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setMessage(e.getMessage());
			response.setStatus(400);
			return new ResponseEntity<Response<Wallet>>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/debitWallet")
	public ResponseEntity<Response<Wallet>> debitWallet(@RequestParam("userId") String userId, @RequestParam("amount") double amount) {
		Response<Wallet> response = new Response<>();
		try {
			Wallet creditedWallet = walletService.debitMoney(userId, amount);
			response.setMessage("Successfully debited the wallet");
			response.setData(creditedWallet);
			response.setStatus(200);
			return new ResponseEntity<Response<Wallet>>(response, HttpStatus.OK);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setMessage(e.getMessage());
			response.setStatus(400);
			return new ResponseEntity<Response<Wallet>>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/getBalance")
	public ResponseEntity<Response<Double>> getBalance(@RequestParam("userId") String userId) {
		Response<Double> response = new Response<>();
		try {
			double currentBalance = walletService.getCurrentBalance(userId);
			response.setMessage("Successfully fetched balance");
			response.setData(currentBalance);
			response.setStatus(200);
			return new ResponseEntity<Response<Double>>(response, HttpStatus.OK);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setMessage(e.getMessage());
			response.setStatus(400);
			return new ResponseEntity<Response<Double>>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/getCreditLogs")
	public ResponseEntity<Response<List<Wallet>>> getCreditLogs(@RequestParam("userId") String userId) {
		Response<List<Wallet>> response = new Response<>();
		try {
			List<Wallet> creditLogs = walletService.getCreditLogs(userId);
			response.setMessage("Successfully fetched balance");
			response.setData(creditLogs);
			response.setStatus(200);
			return new ResponseEntity<Response<List<Wallet>>>(response, HttpStatus.OK);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setMessage(e.getMessage());
			response.setStatus(400);
			return new ResponseEntity<Response<List<Wallet>>>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/getDebitLogs")
	public ResponseEntity<Response<List<Wallet>>> getDebitLogs(@RequestParam("userId") String userId) {
		Response<List<Wallet>> response = new Response<>();
		try {
			List<Wallet> debList = walletService.getDebitLogs(userId);
			response.setMessage("Successfully fetched balance");
			response.setData(debList);
			response.setStatus(200);
			return new ResponseEntity<Response<List<Wallet>>>(response, HttpStatus.OK);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setMessage(e.getMessage());
			response.setStatus(400);
			return new ResponseEntity<Response<List<Wallet>>>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/getAllTransactions")
	public ResponseEntity<Response<List<Wallet>>> getAllTransactions(@RequestParam("userId") String userId) {
		Response<List<Wallet>> response = new Response<>();
		try {
			List<Wallet> allList = walletService.getAllStatements(userId);
			response.setMessage("Successfully fetched balance");
			response.setData(allList);
			response.setStatus(200);
			return new ResponseEntity<Response<List<Wallet>>>(response, HttpStatus.OK);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.setMessage(e.getMessage());
			response.setStatus(400);
			return new ResponseEntity<Response<List<Wallet>>>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
}
