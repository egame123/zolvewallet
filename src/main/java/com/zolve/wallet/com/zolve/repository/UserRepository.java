package com.zolve.repository;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import com.zolve.entity.User;

@Repository
public class UserRepository {
	private final MongoTemplate mongoTemplate;
	
    @Autowired
    public UserRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
    
    public User save(User user) {
    	return mongoTemplate.insert(user);
	}
    
    public User findUserById(String id) {
    	return mongoTemplate.findById(id, User.class);
    }
    
}
