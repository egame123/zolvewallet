# ZolveWallet

Basic Wallet in spring boot and mongodb

Steps to run this application in local:

1. Dowload java and add Path to env variable
2. Add JAVA_HOME env like JAVA_HOME=C:\Program Files (x86)\Java\jdk1.8.0_281
3. Install maven https://mirrors.estointernet.in/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip and add to Path e.g: C:\Users\Nasir\Downloads\apache-maven-3.6.3-bin\apache-maven-3.6.3\bin
4. Go to application.properties file add ur mongodb url
5. cd zolvewallet 
6. mvn clean Install
7. java -jar pathToJarFileWhicGetsCreatedFromAboveCommand e.g: java -jar C:\Users\Nasir\Downloads\zolve-wallet\zolvewallet\target\wallet-0.0.1-SNAPSHOT.jar
8. Create user through postman POST localhost:8085/saveUser body: {"phone": "9596124647"}
9. Create wallet POST localhost:8085/saveWallet?userId=9596124647 

10. Credit Wallet PUT localhost:8085/credWallet?userId=9596124647&amount=1000
11. Debit Wallet PUT localhost:8085/debitWallet?userId=9596124647&amount=1000
12. Get Credit Statement GET localhost:8085/getCreditLogs?userId=9596124647
13. Get Debit Statement GET localhost:8085/getDebitLogs?userId=9596124647
14. Get All Statements GET localhost:8085/getAllTransactions?userId=9596124647
15. Get current balance GET localhost:8085/getBalance?userId=9596124647
